# Lem-in Go Program

## Introduction
This Go program, named "lem-in," is designed to solve graph theory problems related to ant movement optimization.

## Prerequisites
Ensure that you have [Go](https://golang.org/) installed on your system.

## Running the Program
1. Open your terminal.
2. Navigate to the directory containing the `lem-in` program.

   ```bash
   cd /path/to/lem-in
   ```
Run the program using the following command:

```bash
go run . example04.txt
```
Check the example folder in the same repository to know which can be be used. You can also modify them as you want. 
The program will execute, and you should see the output indicating the optimization of ant movement based on the input graph.


###Ensure that your input file follows the required format. For example:

```
4
##start
start_room 0 0
room1 1 0
room2 2 0
room3 3 0
##end
end_room 4 0
start_room-room1
start_room-room2
room1-room3
room2-room3
room3-end_room
```
Modify the input file as needed for your specific use case.

### Additional Notes
This program is designed to handle graph theory problems related to ant movement, so make sure your input adheres to the specified format.
Feel free to customize the program code according to your requirements.

## Stopping the Program
To stop the program, go back to the terminal and press Ctrl + C.

That's it! You've successfully run the lem-in program and optimized ant movement based on the provided input graph.